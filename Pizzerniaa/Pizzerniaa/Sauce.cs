﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerniaa
{
    class Sauce
    {
        public string Name;
        public float Price;

        public void GetSauce()
        {
            Console.WriteLine("Sos: {0}",Name);
            Console.WriteLine("Cena: {0}",Price);
        }
    }
}
