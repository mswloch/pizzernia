﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerniaa
{
    class Program
    {
        static void Main(string[] args)
        {
            List<PIZZA> Menu = new List<PIZZA>();

            PIZZA margaritta = new PIZZA();
            margaritta.Name = "margaritta";
            margaritta.Price = 5f;
            margaritta.Components = "Ser" + " " + "Sos";
            Menu.Add(margaritta);

            PIZZA niedzielna = new PIZZA();
            niedzielna.Name = "niedzielna";
            niedzielna.Price = 10f;
            niedzielna.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Ogórek";
            Menu.Add(niedzielna);

            PIZZA sobotnia = new PIZZA();
            sobotnia.Name = "sobotnia";
            sobotnia.Price = 10f;
            sobotnia.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Pomidor";
            Menu.Add(sobotnia);

            PIZZA piątkowa = new PIZZA();
            piątkowa.Name = "piątkowa";
            piątkowa.Price = 10f;
            piątkowa.Components = "Ser" + " " + "Sos" + " " + "Pieczarki" + " " + "Ogórek";
            Menu.Add(piątkowa);

            PIZZA czwartkowa = new PIZZA();
            czwartkowa.Name = "czwartkowa";
            czwartkowa.Price = 10f;
            czwartkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Kukurudza";
            Menu.Add(czwartkowa);

            PIZZA środowa = new PIZZA();
            środowa.Name = "środowa";
            środowa.Price = 10f;
            środowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Ananas";
            Menu.Add(środowa);

            PIZZA wtorkowa = new PIZZA();
            wtorkowa.Name = "wtorkowa";
            wtorkowa.Price = 10f;
            wtorkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Salami";
            Menu.Add(wtorkowa);

            PIZZA poniedziałkowa = new PIZZA();
            poniedziałkowa.Name = "poniedziałkowa";
            poniedziałkowa.Price = 10f;
            poniedziałkowa.Components = "Ser" + " " + "Sos" + " " + "Szynka" + " " + "Pieczarki";
            Menu.Add(poniedziałkowa);

            PIZZA familijna = new PIZZA();
            familijna.Name = "familijna";
            familijna.Price = 15f;
            familijna.Components = "Ser" + " " + "Sos" + " " + "Kurczak" + " " + "Pieczarki" + " " + "Pomidor" + " " + "Kukurydza";
            Menu.Add(familijna);

            Console.WriteLine("PIZZE:");

            foreach (PIZZA pizza in Menu)
            {
                pizza.GetPizza();
            }

            List<Sauce> SOS = new List<Sauce>();

            Sauce ketchup = new Sauce();
            ketchup.Name = "ketchup";
            ketchup.Price = 1.5f;
            SOS.Add(ketchup);

            Sauce czosnkowy = new Sauce();
            czosnkowy.Name = "czosnkowy";
            czosnkowy.Price = 1.5f;
            SOS.Add(czosnkowy);

            Sauce słodkopikantny = new Sauce();
            słodkopikantny.Name = "słodkopikantny";
            słodkopikantny.Price = 1.5f;
            SOS.Add(słodkopikantny);

            Console.WriteLine("SOSY:");

            foreach (Sauce sos in SOS)
            {
                sos.GetSauce();
            }

            Console.WriteLine("Wybierz pizze");
            string a = Console.ReadLine();
            Console.WriteLine(a.Components);
            


            Console.Read();
        }
    }
}
